package rockPaperScissors;

import java.util.*;
import java.util.function.DoubleToIntFunction;

public class RockPaperScissors {


    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */

        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yesOrNo = Arrays.asList("y", "n");

    public void run() {

    while (true) {
        System.out.println("Let's play round "+roundCounter);


        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        String computerChoice = compChoice(rpsChoices);
        String choices = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);

        if (humanChoice.equals(computerChoice)) {
            System.out.println(choices + " Its a tie!");
            }

        else if (humanChoice.equals("rock")) {
            if (computerChoice.equals("paper")) {
                System.out.println(choices + " Computer wins!");
                computerScore ++;
                }
            else {
                System.out.println(choices + " Human wins!");
                humanScore ++;
                }
            }

        else if (humanChoice.equals("paper")) {
            if (computerChoice.equals("scissors")) {
                System.out.println(choices + " Computer wins");
                computerScore ++;
            }
            else {
                System.out.println(choices + " Human wins!");
                humanScore ++;
                }
            }

        else if (humanChoice.equals("scissors")) {
            if (computerChoice.equals("rock")) {
                System.out.println(choices + " Computer wins");
                computerScore ++;
            }
            else {
                System.out.println(choices + " Human wins!");
                humanScore ++;
                }
            }
        String scores = String.format("Score: human %d, computer %d", humanScore, computerScore);
        System.out.println(scores);
        String continuePlay = continue_playing("Do you wish to continue playing? (y/n)?");

        if (continuePlay.equals("n")) {
            System.out.println("Bye bye :)");
            break;
            }
        roundCounter ++;
            }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        while (true) {
            System.out.println(prompt);
            String userInput = sc.next();
            if (validate_input(userInput, rpsChoices)) {
                return userInput;
            } else {
                System.out.println("I dont understand" + " " + userInput + ". " + "Could you try again?");
            }
        }
    }

    /**
     * Checks if player wants to continue playing.
     * @param prompt
     * @return string input answer from user
     */
    public String continue_playing(String prompt) {
        while (true) {
            System.out.println(prompt);
            String userAnswer = sc.next();
            if (validate_input(userAnswer, yesOrNo)) {
                return userAnswer;
            } else {
                System.out.println("I dont understand" + " " + userAnswer + ". " + "Could you try again?");
            }
        }
    }

    /**
     * chooses random out of rock paper scissors
     * @param list
     * @return random choice.
     */
    public String compChoice(List<String> list) {
        Random r = new Random();
        int randomItem = r.nextInt(list.size());
        String randomElement = list.get(randomItem);
        return randomElement;
    }

    /**
     * Checks if the given input is either rock, paper or scissors.
     * @param input
     * @param validInput
     * @return true if valid, false if not.
     */
    public Boolean validate_input(String input, List<String> validInput) {
        return validInput.contains(input);

    }
}

